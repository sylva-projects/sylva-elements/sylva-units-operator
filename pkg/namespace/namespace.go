/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package namespace

import (
	"maps"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
)

func GenerateNamespace(
	suRelease *sylvav1alpha1.SylvaUnitsRelease,
	helmReleaseNamespace string,
) (corev1.Namespace, error) {

	var specificLabels = map[string]string{
		"external-secret-store": suRelease.Namespace, // Used to match ESO condition
	}
	maps.Copy(specificLabels, common.ResourceLabels)

	namespace := corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   helmReleaseNamespace,
			Labels: specificLabels,
		},
	}

	return namespace, nil
}

func TargetClusterNamespace(suRelease *sylvav1alpha1.SylvaUnitsRelease) string {
	result := ""

	if suRelease.Spec.ClusterType == sylvav1alpha1.WorkloadClusterType {
		result = suRelease.Namespace + "--" + suRelease.Name
	}

	return result
}
