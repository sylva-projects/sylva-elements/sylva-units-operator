/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// +kubebuilder:docs-gen:collapse=Apache License

package namespace

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	surv1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
)

// +kubebuilder:docs-gen:collapse=Imports

var _ = Describe("Namespace package", func() {

	Context("Testing package functions", func() {
		It("Compute and generate the correct workload cluster Helm release namespace for a workload cluster", func() {
			sylvaUnitRelease := common.CreateTestSylvaUnitsRelease()
			sylvaUnitRelease.Spec.ClusterType = surv1alpha1.WorkloadClusterType

			By("Computing the correct Helm release namespace name")
			helmReleaseNamespaceName := TargetClusterNamespace(sylvaUnitRelease)
			Expect(helmReleaseNamespaceName).To(Equal(sylvaUnitRelease.Namespace + "--" + sylvaUnitRelease.Name))

			By("Generating the namespace with correct labels")
			helmReleaseNamespace, _ := GenerateNamespace(sylvaUnitRelease, helmReleaseNamespaceName)
			Expect(helmReleaseNamespace.Name).To(Equal(helmReleaseNamespaceName))
			Expect(helmReleaseNamespace.Labels["external-secret-store"]).To(Equal(sylvaUnitRelease.Namespace))
		})
	})
})
