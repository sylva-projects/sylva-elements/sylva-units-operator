/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package helmrelease

import (
	"encoding/json"
	"maps"
	"time"

	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	helmv2 "github.com/fluxcd/helm-controller/api/v2beta1"
	"github.com/fluxcd/pkg/runtime/transform"
	sourcev1b2 "github.com/fluxcd/source-controller/api/v1beta2"

	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
)

// Types for Values unmarshalling (JSON --> struct)
type capi_providers struct {
	InfraProvider     string `json:"infra_provider"`
	BootstrapProvider string `json:"bootstrap_provider"`
}

type cluster struct {
	CapiProviders capi_providers `json:"capi_providers"`
}

type requiredSUHRValues = struct {
	Cluster cluster `json:"cluster"`
}

// Define a HelmRelease object based on the SylvaUnitsRelease spec
func GenerateHelmRelease(
	suRelease *sylvav1alpha1.SylvaUnitsRelease,
	helmReleaseNamespace string,
	valuesFromList []sylvav1alpha1.ValuesFrom,
) (*helmv2.HelmRelease, error) {

	suRelease = suRelease.DeepCopy()

	// --- Prepare spec.valuesFiles ---
	var valuesFiles []string

	valuesFiles = append(valuesFiles, "values.yaml")

	switch suRelease.Spec.ClusterType {
	case sylvav1alpha1.ManagementClusterType:
		valuesFiles = append(valuesFiles, "management.values.yaml")
	case sylvav1alpha1.WorkloadClusterType:
		valuesFiles = append(valuesFiles, "workload-cluster.values.yaml")
	case sylvav1alpha1.BootstrapClusterType:
		// "bootstrap" here means that we bootstrap a management cluster
		valuesFiles = append(valuesFiles, "management.values.yaml")
		valuesFiles = append(valuesFiles, "bootstrap.values.yaml")
	}

	// here we adapt to the fact that the file location will be different
	// depending on whether the Helm chart is read from sylva-core Git repo charts/sylva-units
	// or from the OCI artifact
	if suRelease.Spec.SylvaUnitsSource.Type == sylvav1alpha1.SourceTypeGit {
		for i, file := range valuesFiles {
			valuesFiles[i] = "charts/sylva-units/" + file
		}
	}

	// if the
	if suRelease.Spec.SylvaUnitsSource.Type == sylvav1alpha1.SourceTypeOCI {
		valuesFiles = append(valuesFiles, "use-oci-artifacts.values.yaml")
	}

	// Add valuesFrom from spec.valuesFrom (computed by the workload cluster operator)
	valuesFrom := []helmv2.ValuesReference{}

	if suRelease.Spec.ValuesFrom != nil {
		for _, suValuesFromItem := range suRelease.Spec.ValuesFrom {
			var valuesFromItem helmv2.ValuesReference

			valuesFromItem.Optional = suValuesFromItem.Optional

			if suValuesFromItem.TargetPath != "" {
				valuesFromItem.TargetPath = suValuesFromItem.TargetPath
			}

			if suValuesFromItem.ValuesKey != "" {
				valuesFromItem.ValuesKey = suValuesFromItem.ValuesKey
			}

			switch suValuesFromItem.Type {
			case sylvav1alpha1.ValuesFromTypeConfigMap,
				sylvav1alpha1.ValuesFromTypeSecret:
				valuesFromItem.Kind = suValuesFromItem.Type
				valuesFromItem.Name = suValuesFromItem.Name
			}

			valuesFrom = append(valuesFrom, valuesFromItem)
		}
	}

	// Add valuesFrom from valuesFromList (computed by the present operator)
	for _, suValuesFromItem := range valuesFromList {
		var valuesFromItem helmv2.ValuesReference

		valuesFromItem.Optional = suValuesFromItem.Optional

		if suValuesFromItem.TargetPath != "" {
			valuesFromItem.TargetPath = suValuesFromItem.TargetPath
		}

		if suValuesFromItem.ValuesKey != "" {
			valuesFromItem.ValuesKey = suValuesFromItem.ValuesKey
		}

		switch suValuesFromItem.Type {
		case sylvav1alpha1.ValuesFromTypeConfigMap,
			sylvav1alpha1.ValuesFromTypeSecret:
			valuesFromItem.Kind = suValuesFromItem.Type
			valuesFromItem.Name = suValuesFromItem.Name
		}

		valuesFrom = append(valuesFrom, valuesFromItem)
	}

	// --- adapt chart path, chart kind, version and reconcileStrategy depending of source type

	chartPath := "sylva-units"
	var chartKind string
	var reconcileStrategy string
	var chartVersion string
	if suRelease.Spec.SylvaUnitsSource.Type == sylvav1alpha1.SourceTypeGit {
		chartPath = "charts/" + chartPath
		chartKind = "GitRepository"
		reconcileStrategy = sourcev1b2.ReconcileStrategyRevision
		chartVersion = "*"
	} else { // SourceTypeOCI
		chartKind = "HelmRepository"
		reconcileStrategy = sourcev1b2.ReconcileStrategyChartVersion
		chartVersion = suRelease.Spec.SylvaUnitsSource.Tag
	}

	// --- Produce HelmRelease definition ---

	var specificLabels = map[string]string{}
	maps.Copy(specificLabels, common.ResourceLabels)

	helmRelease := &helmv2.HelmRelease{
		ObjectMeta: metav1.ObjectMeta{
			Name:      sylvav1alpha1.BaseSylvaUnitsResourceName,
			Namespace: helmReleaseNamespace,
			Labels:    specificLabels,
		},
		Spec: helmv2.HelmReleaseSpec{
			Chart: helmv2.HelmChartTemplate{
				Spec: helmv2.HelmChartTemplateSpec{
					SourceRef: helmv2.CrossNamespaceObjectReference{
						Kind: chartKind,
						Name: sylvav1alpha1.BaseSylvaUnitsResourceName,
					},
					Chart:             chartPath,
					ValuesFiles:       valuesFiles,
					ReconcileStrategy: reconcileStrategy,
					Version:           chartVersion,
				},
			},
			Values:     suRelease.Spec.Values,
			ValuesFrom: valuesFrom,
			Interval:   suRelease.Spec.Interval,
			Suspend:    suRelease.Spec.Suspend,
		},
	}

	// --- Update the workload cluster Helm release timeout ---

	var suhrValues requiredSUHRValues
	if err := json.Unmarshal(helmRelease.Spec.Values.Raw, &suhrValues); err != nil {
		return nil, err
	}

	if suRelease.Spec.ClusterType == sylvav1alpha1.WorkloadClusterType {
		if suhrValues.Cluster.CapiProviders.InfraProvider != "capm3" {
			helmRelease.Spec.Uninstall = &helmv2.Uninstall{
				Timeout: &metav1.Duration{Duration: 25 * time.Minute},
			}
		} else {
			helmRelease.Spec.Uninstall = &helmv2.Uninstall{
				Timeout: &metav1.Duration{Duration: 80 * time.Minute},
			}
		}
	}
	if suRelease.Spec.SylvaUnitsSource.Type == sylvav1alpha1.SourceTypeGit {
		ref := map[string]interface{}{}
		if suRelease.Spec.SylvaUnitsSource.Tag != "" || suRelease.Spec.SylvaUnitsSource.Branch != "" {
			ref["commit"] = nil // Prevent passing the default non real commit of the Sylva Helm Release
			if suRelease.Spec.SylvaUnitsSource.Tag != "" {
				ref["tag"] = suRelease.Spec.SylvaUnitsSource.Tag
			}
			if suRelease.Spec.SylvaUnitsSource.Branch != "" {
				ref["branch"] = suRelease.Spec.SylvaUnitsSource.Branch
			}
		} else {
			// It is guaranteed by an XValidation rule that at least one of commit / tag / branch is set
			ref["commit"] = suRelease.Spec.SylvaUnitsSource.Commit
		}

		addSourceTemplates := map[string]interface{}{
			"source_templates": map[string]interface{}{
				"sylva-core": map[string]interface{}{
					"kind": "GitRepository",
					"spec": map[string]interface{}{
						"url": suRelease.Spec.SylvaUnitsSource.URL,
						"ref": ref,
					},
				},
			},
		}
		raw, err := json.Marshal(transform.MergeMaps(addSourceTemplates, suRelease.GetValues()))
		if err != nil {
			return nil, err
		}
		if helmRelease.Spec.Values == nil {
			helmRelease.Spec.Values = &apiextensionsv1.JSON{}
		}
		helmRelease.Spec.Values.Raw = raw
	}
	return helmRelease, nil
}
