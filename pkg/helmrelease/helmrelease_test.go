/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package helmrelease

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"encoding/json"
	"time"

	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
)

// +kubebuilder:docs-gen:collapse=Imports

var _ = Describe("Helmrelease package", func() {
	Context("Testing package functions", func() {
		It("Compute and generate the correct Helm release for a workload cluster", func() {
			By("Computing the correct Helm release for a git source")
			suRelease := common.CreateTestSylvaUnitsRelease()
			suRelease.Spec.ClusterType = sylvav1alpha1.WorkloadClusterType
			helmReleaseNamespace := suRelease.Namespace + "--" + suRelease.Name
			valuesFromList := []sylvav1alpha1.ValuesFrom{}
			helmRelease, err := GenerateHelmRelease(
				suRelease,
				helmReleaseNamespace,
				valuesFromList,
			)

			Expect(err).To(BeNil())
			Expect(helmRelease.Spec.Chart.Spec.SourceRef.Kind).To(Equal("GitRepository"))
			Expect(helmRelease.Spec.Chart.Spec.Chart).To(Equal("charts/sylva-units"))
			Expect(helmRelease.Spec.Uninstall.Timeout.Duration).To(Equal(25 * time.Minute))

			By("Computing the correct Helm release for an OCI source")
			suRelease.Spec.SylvaUnitsSource.Type = "oci"
			suRelease.Spec.SylvaUnitsSource.URL = "oci://foo.bar"
			helmRelease, err = GenerateHelmRelease(
				suRelease,
				helmReleaseNamespace,
				valuesFromList,
			)
			Expect(err).To(BeNil())
			Expect(helmRelease.Spec.Chart.Spec.SourceRef.Kind).To(Equal("HelmRepository"))
			Expect(helmRelease.Spec.Chart.Spec.Chart).To(Equal("sylva-units"))

			By("Computing the correct Helm release timeout for capm3 workload cluster")
			suRelease = common.CreateTestSylvaUnitsRelease()
			suRelease.Spec.ClusterType = sylvav1alpha1.WorkloadClusterType
			helmReleaseNamespace = suRelease.Namespace + "--" + suRelease.Name
			suReleaseClusterSettings, _ := json.Marshal(map[string]any{
				"cluster": map[string]any{
					"capi_providers": map[string]any{
						"bootstrap_provider": "cabpr",
						"infra_provider":     "capm3",
					},
				},
			})
			suRelease.Spec.Values.Raw = suReleaseClusterSettings
			helmRelease, err = GenerateHelmRelease(
				suRelease,
				helmReleaseNamespace,
				valuesFromList,
			)
			Expect(err).To(BeNil())
			Expect(helmRelease.Spec.Uninstall.Timeout.Duration).To(Equal(80 * time.Minute))

		})
	})
})
