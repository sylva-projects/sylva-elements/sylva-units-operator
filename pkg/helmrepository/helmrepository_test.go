/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package helmrepository

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	surv1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
)

// +kubebuilder:docs-gen:collapse=Imports

var _ = Describe("Helmrepository package", func() {

	Context("Testing package functions", func() {
		It("Compute and generate the correct Flux gitrepository", func() {
			sylvaUnitsRelease := common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Spec.SylvaUnitsSource.Type = "oci"
			sylvaUnitsRelease.Spec.SylvaUnitsSource.URL = "oci://foo.bar"
			helmReleaseNamespace := sylvaUnitsRelease.Namespace + "--" + sylvaUnitsRelease.Name

			By("Ensuring the Helm repository settings are correct")
			helmRepository, err := GenerateHelmRepository(sylvaUnitsRelease, helmReleaseNamespace)
			Expect(err).To(BeNil())
			Expect(helmRepository.Name).To(Equal(surv1alpha1.BaseSylvaUnitsResourceName))
			Expect(helmRepository.Spec.URL).To(Equal(sylvaUnitsRelease.Spec.SylvaUnitsSource.URL))
		})

		It("Handle wrong settings", func() {
			By("Failing for an empty type reference")
			sylvaUnitsRelease := common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Spec.SylvaUnitsSource.Type = ""
			sylvaUnitsRelease.Spec.SylvaUnitsSource.URL = "oci://foo.bar"
			helmReleaseNamespace := sylvaUnitsRelease.Namespace + "--" + sylvaUnitsRelease.Name
			_, err := GenerateHelmRepository(sylvaUnitsRelease, helmReleaseNamespace)
			Expect(err).To(Not(BeNil()))
		})
	})
})
