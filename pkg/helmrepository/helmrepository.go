/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package helmrepository

import (
	"errors"
	"maps"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"

	sourcev1b2 "github.com/fluxcd/source-controller/api/v1beta2"
)

// Define a HelmRepository object based on a SylvaUnitsRelease spec
func GenerateHelmRepository(
	suRelease *sylvav1alpha1.SylvaUnitsRelease,
	helmReleaseNamespace string,
) (*sourcev1b2.HelmRepository, error) {

	if suRelease.Spec.SylvaUnitsSource.Type != sylvav1alpha1.SourceTypeOCI {
		return nil, errors.New("HelmRepository is only relevant to generate for a SylvaUnitsRelease of type OCI")
	}

	var specificLabels = map[string]string{}
	maps.Copy(specificLabels, common.ResourceLabels)

	helmRepository := &sourcev1b2.HelmRepository{
		ObjectMeta: metav1.ObjectMeta{
			Name:      sylvav1alpha1.BaseSylvaUnitsResourceName,
			Namespace: helmReleaseNamespace,
			Labels:    specificLabels,
		},
		Spec: sourcev1b2.HelmRepositorySpec{
			Type:     sourcev1b2.HelmRepositoryTypeOCI,
			URL:      suRelease.Spec.SylvaUnitsSource.URL,
			Interval: suRelease.Spec.Interval,
		},
	}

	return helmRepository, nil
}
