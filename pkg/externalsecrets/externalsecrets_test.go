/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package externalsecrets

import (
	"fmt"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"

	esov1b1 "github.com/external-secrets/external-secrets/apis/externalsecrets/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +kubebuilder:docs-gen:collapse=Imports

type testValues struct {
	ClusterStoreNamespace string
	Namespace             string
	TargetName            string
	InLineTemplate        string
	RefreshInterval       metav1.Duration
	ExternalPath          string
	ExternalKey           string
}

func generateTestValues() testValues {
	result := testValues{
		ClusterStoreNamespace: "team",
		Namespace:             "team",
		TargetName:            sylvav1alpha1.BaseSylvaUnitsResourceName,
		InLineTemplate:        `{{- (regexReplaceAll "{{.*}}" .secrets "")` + ` | fromJson | toYaml }}`,
		RefreshInterval:       metav1.Duration{Duration: 30 * time.Second},
		ExternalPath:          "common",
		ExternalKey:           "capo",
	}

	return result
}

func generateTestExternalSecret(in testValues) (*esov1b1.ExternalSecret, error) {
	testExternalSecret, err := GenerateExternalSecret(
		in.ClusterStoreNamespace,
		in.Namespace,
		in.TargetName,
		in.InLineTemplate,
		in.RefreshInterval,
		in.ExternalPath,
		in.ExternalKey,
	)

	if err != nil {
		return nil, err
	} else {
		return testExternalSecret, nil
	}
}

var _ = Describe("Externalsecrets package", func() {

	Context("Testing package functions", func() {

		It("Compute and generate correct external secrets", func() {
			By("Computing a correct external secret in the same namespace than the secret cluster store")
			test := generateTestValues()
			externalSecret, err := generateTestExternalSecret(test)
			Expect(err).To(BeNil())
			Expect(externalSecret.Namespace).To(Equal(test.Namespace))
			Expect(externalSecret.Spec.SecretStoreRef.Name).To(Equal(test.ClusterStoreNamespace))
			Expect(externalSecret.Spec.SecretStoreRef.Kind).To(Equal("ClusterSecretStore"))
			Expect(externalSecret.Spec.Data[0].RemoteRef.Key).To(Equal(fmt.Sprintf("secret/data/%s/%s/%s",
				test.ClusterStoreNamespace,
				test.ExternalPath,
				test.ExternalKey,
			)))

			By("Computing a correct external secret in another namespace than the secret cluster sore")
			test = generateTestValues()
			test.Namespace = "team-test"
			externalSecret, err = generateTestExternalSecret(test)
			Expect(err).To(BeNil())
			Expect(externalSecret.Namespace).To(Equal(test.Namespace))

			By("Computing a correct external secret even without a path")
			test = generateTestValues()
			test.ExternalPath = ""
			test.ExternalKey = "capo"
			externalSecret, err = generateTestExternalSecret(test)
			Expect(err).To(BeNil())
			Expect(externalSecret.Spec.Data[0].RemoteRef.Key).To(Equal(fmt.Sprintf("secret/data/%s/%s",
				test.ClusterStoreNamespace,
				test.ExternalKey,
			)))

			By("Computing a correct external secret even without a key")
			test = generateTestValues()
			test.ExternalPath = "common"
			test.ExternalKey = ""
			externalSecret, err = generateTestExternalSecret(test)
			Expect(err).To(BeNil())
			Expect(externalSecret.Spec.Data[0].RemoteRef.Key).To(Equal(fmt.Sprintf("secret/data/%s/%s",
				test.ClusterStoreNamespace,
				test.ExternalPath,
			)))

			By("Failing if there is no path nor key")
			test = generateTestValues()
			test.ExternalPath = ""
			test.ExternalKey = ""
			_, err = generateTestExternalSecret(test)
			Expect(err).To(Not(BeNil()))
		})
	})
})
