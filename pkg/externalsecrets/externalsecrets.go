/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package externalsecrets

import (
	"fmt"
	"maps"

	esov1b1 "github.com/external-secrets/external-secrets/apis/externalsecrets/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
)

// Define an external secret from the SecretValues field
func GenerateExternalSecret(
	esClusterStoreNamespace string,
	esNamespace string,
	esTargetName string,
	esInLineTemplate string,
	esRefreshInterval metav1.Duration,
	esExternalPath string,
	esExternalKey string,
) (*esov1b1.ExternalSecret, error) {

	var specificLabels = map[string]string{}
	maps.Copy(specificLabels, common.ResourceLabels)

	secretPath := ""

	if esExternalPath == "" && esExternalKey == "" {
		return nil, fmt.Errorf("no path nor key has been defined in the secretvalues")
	} else if esExternalPath == "" || esExternalKey == "" {
		secretPath = fmt.Sprintf("secret/data/%s/%s%s",
			esClusterStoreNamespace,
			esExternalPath,
			esExternalKey,
		)
	} else {
		secretPath = fmt.Sprintf("secret/data/%s/%s/%s",
			esClusterStoreNamespace,
			esExternalPath,
			esExternalKey,
		)
	}

	externalSecret := &esov1b1.ExternalSecret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      sylvav1alpha1.BaseSylvaUnitsResourceName,
			Namespace: esNamespace,
			Labels:    specificLabels,
		},
		Spec: esov1b1.ExternalSecretSpec{
			SecretStoreRef: esov1b1.SecretStoreRef{
				Name: esClusterStoreNamespace,
				Kind: "ClusterSecretStore",
			},
			Target: esov1b1.ExternalSecretTarget{
				Name: esTargetName,
				Template: &esov1b1.ExternalSecretTemplate{
					EngineVersion: esov1b1.TemplateEngineV2,
					Data: map[string]string{
						"secrets": esInLineTemplate,
					},
				},
			},
			Data: []esov1b1.ExternalSecretData{
				{
					SecretKey: "secrets",
					RemoteRef: esov1b1.ExternalSecretDataRemoteRef{
						Key: secretPath,
					},
				},
			},
			RefreshInterval: &esRefreshInterval,
		},
	}

	return externalSecret, nil
}
