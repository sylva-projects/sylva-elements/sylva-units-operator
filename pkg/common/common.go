/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package common

import (
	"encoding/json"

	surv1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var ResourceLabels = map[string]string{
	"app.kubernetes.io/managed-by": "Operator",
	"app.kubernetes.io/created-by": "Sylva-units-operator",
}

func CreateTestSylvaUnitsRelease() *surv1alpha1.SylvaUnitsRelease {

	simpleClusterSettings, _ := json.Marshal(map[string]any{
		"cluster": map[string]any{
			"capi_providers": map[string]any{
				"bootstrap_provider": "cabpr",
				"infra_provider":     "capd",
			},
		},
	})

	sylvaUnitRelease := &surv1alpha1.SylvaUnitsRelease{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "test",
			Namespace: "team",
		},
		Spec: surv1alpha1.SylvaUnitsReleaseSpec{
			ClusterType: "management",
			SylvaUnitsSource: &surv1alpha1.SylvaUnitsSource{
				Type:   "git",
				URL:    "https://gitlab.com/sylva-projects/sylva-core.git",
				Branch: "main",
			},
			Values: &apiextensionsv1.JSON{
				Raw: simpleClusterSettings,
			},
		},
	}
	return sylvaUnitRelease
}
