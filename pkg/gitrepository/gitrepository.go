/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package gitrepository

import (
	"errors"
	"maps"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"

	sourcev1 "github.com/fluxcd/source-controller/api/v1"
)

// Define a GitRepository object based on a SylvaUnitsRelease spec
func GenerateGitRepository(
	suRelease *sylvav1alpha1.SylvaUnitsRelease,
	helmReleaseNamespace string,
) (*sourcev1.GitRepository, error) {

	if suRelease.Spec.SylvaUnitsSource.Type != sylvav1alpha1.SourceTypeGit {
		return nil, errors.New("GitRepository is only relevant to generate for a SylvaUnitsRelease of type Git")
	}

	var specificLabels = map[string]string{}
	maps.Copy(specificLabels, common.ResourceLabels)

	gitRepository := &sourcev1.GitRepository{
		ObjectMeta: metav1.ObjectMeta{
			Name:      sylvav1alpha1.BaseSylvaUnitsResourceName,
			Namespace: helmReleaseNamespace,
			Labels:    specificLabels,
		},
		Spec: sourcev1.GitRepositorySpec{
			URL:      suRelease.Spec.SylvaUnitsSource.URL,
			Interval: suRelease.Spec.Interval,
			Reference: &sourcev1.GitRepositoryRef{
				Commit: suRelease.Spec.SylvaUnitsSource.Commit,
				Tag:    suRelease.Spec.SylvaUnitsSource.Tag,
				Branch: suRelease.Spec.SylvaUnitsSource.Branch,
			},
		},
	}

	return gitRepository, nil
}
