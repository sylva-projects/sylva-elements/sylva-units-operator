/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// +kubebuilder:docs-gen:collapse=Apache License

package gitrepository

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	surv1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
)

// +kubebuilder:docs-gen:collapse=Imports

var _ = Describe("Gitrepository package", func() {

	Context("Testing package functions", func() {

		It("Compute and generate the correct Flux gitrepository", func() {
			sylvaUnitsRelease := common.CreateTestSylvaUnitsRelease()
			helmReleaseNamespace := sylvaUnitsRelease.Namespace + "--" + sylvaUnitsRelease.Name

			By("Ensuring the Git repository settings are correct")
			gitRepository, err := GenerateGitRepository(sylvaUnitsRelease, helmReleaseNamespace)
			Expect(err).To(BeNil())
			Expect(gitRepository.Name).To(Equal(surv1alpha1.BaseSylvaUnitsResourceName))
			Expect(gitRepository.Spec.URL).To(Equal(sylvaUnitsRelease.Spec.SylvaUnitsSource.URL))
			Expect(gitRepository.Spec.Reference.Branch).To(Equal(sylvaUnitsRelease.Spec.SylvaUnitsSource.Branch))
		})

		It("Handle wrong settings", func() {
			By("Failing for an empty type reference")
			sylvaUnitsRelease := common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Spec.SylvaUnitsSource.Type = ""
			helmReleaseNamespace := sylvaUnitsRelease.Namespace + "--" + sylvaUnitsRelease.Name
			_, err := GenerateGitRepository(sylvaUnitsRelease, helmReleaseNamespace)
			Expect(err).To(Not(BeNil()))
		})
	})
})
