/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// +kubebuilder:docs-gen:collapse=Apache License

package configmap

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +kubebuilder:docs-gen:collapse=Imports
var _ = Describe("Namespace package", func() {

	Context("Testing package functions", func() {
		It("Copy configmap across namespace", func() {
			sourceConfigMap := corev1.ConfigMap{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "sourceName",
					Namespace: "sourceNamespace",
				},
				Data: map[string]string{
					"foo":  "bar",
					"toto": "tata",
				},
			}
			targetNamespace := "targetNamespace"

			targetConfigMap, err := CopyBetweenNamespace(&sourceConfigMap, targetNamespace)
			Expect(err).To(BeNil())
			Expect(targetConfigMap.Namespace).To(Equal(targetNamespace))
			for k := range sourceConfigMap.Data {
				Expect(targetConfigMap.Data[k]).To(Equal(sourceConfigMap.Data[k]))
			}

		})
	})
})
