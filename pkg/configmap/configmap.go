/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package configmap

import (
	"maps"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
)

func CopyBetweenNamespace(sourceConfigMap *corev1.ConfigMap, destinationNamespace string) (*corev1.ConfigMap, error) {

	var specificLabels = map[string]string{}
	maps.Copy(specificLabels, common.ResourceLabels)

	destinationConfigMap := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      sourceConfigMap.Name,
			Namespace: destinationNamespace,
			Labels:    common.ResourceLabels,
		},
		Data: sourceConfigMap.Data,
	}

	return destinationConfigMap, nil
}
