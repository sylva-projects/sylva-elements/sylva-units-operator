/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
// +kubebuilder:docs-gen:collapse=Apache License

/*
Ideally, we should have one `<kind>_controller_test.go` for each controller scaffolded and called in the `suite_test.go`.
So, let's write our example test for the SylvaWorkloadCluster controller (`sylvaworkloadcluster_controller_test.go.`)
*/

/*
As usual, we start with the necessary imports. We also define some utility variables.
*/
package controller

import (
	"context"
	"time"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	. "github.com/onsi/gomega/gstruct"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	surv1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/common"
	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
)

// +kubebuilder:docs-gen:collapse=Imports

var (
	// Common timeouts and retry intervals
	timeout  = time.Second * 30
	interval = time.Millisecond * 250
)

func createSylvaUnitsReleaseNamespace(sylvaUnitsRelease *surv1alpha1.SylvaUnitsRelease) *corev1.Namespace {

	surNamespace := corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: sylvaUnitsRelease.Namespace,
		},
	}
	return &surNamespace
}

func createManagedWCSettings(sylvaUnitsRelease *surv1alpha1.SylvaUnitsRelease) *corev1.ConfigMap {
	managedWorkloadClustersSettings := corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "managed-workload-clusters-settings",
			Namespace: sylvaUnitsRelease.Namespace,
		},
	}
	return &managedWorkloadClustersSettings
}

var _ = Describe("SylvaUnitsRelease controller", func() {

	ctx := context.Background()

	Context("When setting up the test environment", func() {

		It("Creates basic workload cluster SUR resources, without secretValues", func() {
			sylvaUnitsRelease := common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Name = "wc1"
			sylvaUnitsRelease.Spec.ClusterType = surv1alpha1.WorkloadClusterType
			sylvaUnitsReleaseNamespace := createSylvaUnitsReleaseNamespace(sylvaUnitsRelease)
			managedWorkloadClustersSettings := createManagedWCSettings(sylvaUnitsRelease)
			Expect(k8sClient.Create(ctx, sylvaUnitsReleaseNamespace)).Should(Succeed())
			Expect(k8sClient.Create(ctx, managedWorkloadClustersSettings)).Should(Succeed())
			Expect(k8sClient.Create(ctx, sylvaUnitsRelease)).Should(Succeed())

			By("Getting the previously created resource")
			sylvaUnitsReleaseNamspacedName := types.NamespacedName{Name: sylvaUnitsRelease.Name, Namespace: sylvaUnitsRelease.Namespace}
			createdSylvaUnitsRelease := &surv1alpha1.SylvaUnitsRelease{}
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaUnitsReleaseNamspacedName, createdSylvaUnitsRelease)).To(Succeed())
			}, timeout, interval).Should(Succeed())

			By("Waiting for Sylva units release to have a Ready condition set")
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaUnitsReleaseNamspacedName, createdSylvaUnitsRelease)).To(Succeed())
				g.Expect(createdSylvaUnitsRelease.Status.Conditions).To(ContainElement(
					MatchFields(IgnoreExtras, Fields{"Type": Equal("Ready")})))
			}, timeout, interval).Should(Succeed())

			By("Removing it")
			Expect(k8sClient.Delete(ctx, sylvaUnitsRelease)).Should(Succeed())
			// The workload cluster namespace can not be deleted within the k8s emulation. See https://book.kubebuilder.io/reference/envtest.html#namespace-usage-limitation
			// Hence, it is not possible to wait for the removal the sylva units release
		})

		It("Creates workload cluster SUR resources with secretValues", func() {
			sylvaUnitsRelease := common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Name = "wc2"
			sylvaUnitsRelease.Spec.ClusterType = surv1alpha1.WorkloadClusterType
			sylvaUnitsRelease.Spec.SecretValues = surv1alpha1.SecretValuesSpec{
				Path: "common",
				Key:  "capo",
			}
			Expect(k8sClient.Create(ctx, sylvaUnitsRelease)).Should(Succeed())

			By("Getting the previously created resource")
			sylvaUnitsReleaseNamspacedName := types.NamespacedName{Name: sylvaUnitsRelease.Name, Namespace: sylvaUnitsRelease.Namespace}
			createdSylvaUnitsRelease := &surv1alpha1.SylvaUnitsRelease{}
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(
					ctx, sylvaUnitsReleaseNamspacedName, createdSylvaUnitsRelease)).To(Succeed())
			}, timeout, interval).Should(Succeed())

			By("Waiting for Sylva units release to have a Ready condition set")
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaUnitsReleaseNamspacedName, createdSylvaUnitsRelease)).To(Succeed())
				g.Expect(createdSylvaUnitsRelease.Status.Conditions).To(ContainElement(
					MatchFields(IgnoreExtras, Fields{"Type": Equal("Ready")})))
			}, timeout, interval).Should(Succeed())

			By("Removing it")
			Expect(k8sClient.Delete(ctx, sylvaUnitsRelease)).Should(Succeed())
			// The workload cluster namespace can not be deleted within the k8s emulation. See https://book.kubebuilder.io/reference/envtest.html#namespace-usage-limitation
			// Hence, it is not possible to wait for the removal the sylva units release
		})

		It("Does not create wrong workload cluster SUR resources", func() {
			By("Not creating SUR with wrong ClusterType")
			sylvaUnitsRelease := common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Spec.ClusterType = "toto"
			Expect(k8sClient.Create(ctx, sylvaUnitsRelease)).To(Not(Succeed()))

			By("Not creating SUR with wrong SylvaUnitsSource - no tag, no commit nor branch")
			sylvaUnitsRelease = common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Spec.SylvaUnitsSource = &surv1alpha1.SylvaUnitsSource{
				Type: "git",
				URL:  "https://foo.bar",
			}
			Expect(k8sClient.Create(ctx, sylvaUnitsRelease)).To(Not(Succeed()))

			By("Not creating SUR with wrong SylvaUnitsSource - wrong scheme")
			sylvaUnitsRelease = common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Spec.SylvaUnitsSource = &surv1alpha1.SylvaUnitsSource{
				Type: "git",
				URL:  "ftps://foo.bar",
				Tag:  "0.1.2",
			}
			Expect(k8sClient.Create(ctx, sylvaUnitsRelease)).To(Not(Succeed()))
		})
	})

	Context("When setting up the test environment where the required CRD will be removed", func() {
		It("Removes the required CRD", func() {
			By("Deleting the required CRD")
			CRDNames := []string{
				"externalsecrets.external-secrets.io",
				"helmreleases.helm.toolkit.fluxcd.io",
				"kustomizations.kustomize.toolkit.fluxcd.io",
				"gitrepositories.source.toolkit.fluxcd.io",
				"helmcharts.source.toolkit.fluxcd.io",
				"helmrepositories.source.toolkit.fluxcd.io",
				"ocirepositories.source.toolkit.fluxcd.io",
			}
			for _, CRDName := range CRDNames {
				CRDNamspacedName := types.NamespacedName{Name: CRDName}
				CRD := &apiextensionsv1.CustomResourceDefinition{}
				Expect(k8sClient.Get(ctx, CRDNamspacedName, CRD)).To(Succeed())
				Expect(k8sClient.Delete(ctx, CRD)).To(Succeed())
			}
		})

		It("Still can create and remove a simple SylvaUnitsRelease resources", func() {
			By("Creating a simple workload cluster SUR resource")
			sylvaUnitsRelease := common.CreateTestSylvaUnitsRelease()
			sylvaUnitsRelease.Name = "wc3"
			sylvaUnitsRelease.Spec.ClusterType = surv1alpha1.WorkloadClusterType
			Expect(k8sClient.Create(ctx, sylvaUnitsRelease)).Should(Succeed())

			By("Getting the previously created resource")
			sylvaUnitsReleaseNamspacedName := types.NamespacedName{Name: sylvaUnitsRelease.Name, Namespace: sylvaUnitsRelease.Namespace}
			createdSylvaUnitsRelease := &surv1alpha1.SylvaUnitsRelease{}
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(
					ctx, sylvaUnitsReleaseNamspacedName, createdSylvaUnitsRelease)).To(Succeed())
			}, timeout, interval).Should(Succeed())

			By("Waiting for Sylva units release to have a Ready condition set")
			Eventually(func(g Gomega) {
				g.Expect(k8sClient.Get(ctx, sylvaUnitsReleaseNamspacedName, createdSylvaUnitsRelease)).To(Succeed())
				g.Expect(createdSylvaUnitsRelease.Status.Conditions).To(ContainElement(
					MatchFields(IgnoreExtras, Fields{"Type": Equal("Ready")})))
			}, timeout, interval).Should(Succeed())

			By("Removing it")
			Expect(k8sClient.Delete(ctx, sylvaUnitsRelease)).Should(Succeed())
			// The workload cluster namespace can not be deleted within the k8s emulation. See https://book.kubebuilder.io/reference/envtest.html#namespace-usage-limitation
			// Hence, it is not possible to wait for the removal the sylva units release
		})
	})
})
