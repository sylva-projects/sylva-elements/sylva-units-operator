/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	"time"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	kerrors "k8s.io/apimachinery/pkg/util/errors"
	"k8s.io/client-go/tools/record"
	"sigs.k8s.io/cli-utils/pkg/kstatus/status"
	ctrl "sigs.k8s.io/controller-runtime"
	client "sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	esov1b1 "github.com/external-secrets/external-secrets/apis/externalsecrets/v1beta1"
	helmv2 "github.com/fluxcd/helm-controller/api/v2beta1"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"
	"github.com/fluxcd/pkg/runtime/conditions"
	"github.com/fluxcd/pkg/runtime/patch"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	sourcev1b2 "github.com/fluxcd/source-controller/api/v1beta2"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	sylvav1alpha1 "gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/api/v1alpha1"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/configmap"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/externalsecrets"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/gitrepository"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/helmrelease"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/helmrepository"
	"gitlab.com/sylva-projects/sylva-elements/sylva-units-operator/pkg/namespace"
)

const (
	resourcePollingInterval = 5 * time.Second
	fieldOwner              = "sylva-units-release-controller"
	sylvaUnitsStatusName    = "sylva-units-status"
)

// SylvaUnitsReleaseReconciler reconciles a SylvaUnitsRelease object
type SylvaUnitsReleaseReconciler struct {
	client.Client
	Scheme *runtime.Scheme
	record.EventRecorder
}

// +kubebuilder:rbac:groups=unitsoperator.sylva,resources=sylvaunitsreleases,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=unitsoperator.sylva,resources=sylvaunitsreleases/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=unitsoperator.sylva,resources=sylvaunitsreleases/finalizers,verbs=update
// +kubebuilder:rbac:groups=heatoperator.sylva,resources=heatstacks,verbs=get;list;watch

// +kubebuilder:rbac:groups=helm.toolkit.fluxcd.io,resources=helmreleases,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=source.toolkit.fluxcd.io,resources=gitrepositories,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=source.toolkit.fluxcd.io,resources=helmrepositories,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=kustomize.toolkit.fluxcd.io,resources=kustomizations,verbs=get;list;watch
// +kubebuilder:rbac:groups=external-secrets.io,resources=externalsecrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=namespaces,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=cluster.x-k8s.io,resources=clusters,verbs=get;list;watch

func (r *SylvaUnitsReleaseReconciler) Reconcile(ctx context.Context, req ctrl.Request) (result ctrl.Result, retErr error) {
	log := ctrl.LoggerFrom(ctx)
	log.Info("Reconcile SylvaUnitsRelease")

	suRelease := &sylvav1alpha1.SylvaUnitsRelease{}
	if err := r.Get(ctx, req.NamespacedName, suRelease); err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}
	// Initialize the patch helper.
	patchHelper := patch.NewSerialPatcher(suRelease, r.Client)

	// Always attempt to Patch the Cluster object and status after each reconciliation.
	defer func() {
		patchOpts := []patch.Option{}
		if retErr != nil {
			conditions.MarkFalse(suRelease, sylvav1alpha1.ReadyCondition, sylvav1alpha1.ReconciliationFailedReason, "Failed to reconcile sylvaUnitsRelease: %s", retErr)
		} else if conditions.IsTrue(suRelease, sylvav1alpha1.ReadyCondition) && suRelease.Status.ObservedGeneration != suRelease.Generation {
			// Patch ObservedGeneration as the reconciliation completed successfully
			r.Eventf(suRelease, "Normal", "ReconciliationSucceeded", "Successfully reconciled SylvaUnitsRelease %s", suRelease.Name)
			patchOpts = append(patchOpts, patch.WithStatusObservedGeneration{})
		}
		if err := patchHelper.Patch(ctx, suRelease, patchOpts...); err != nil {
			ferr := fmt.Errorf("failed to patch: %w", err)
			retErr = kerrors.NewAggregate([]error{retErr, ferr})
		}
	}()

	// Handle deletion reconciliation loop.
	if !suRelease.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.reconcileDelete(ctx, suRelease)
	}

	// Add finalizer here as it can only be added when the deletionTimestamp is not set.
	if !controllerutil.ContainsFinalizer(suRelease, sylvav1alpha1.SylvaUnitsFinalizer) {
		controllerutil.AddFinalizer(suRelease, sylvav1alpha1.SylvaUnitsFinalizer)
		return ctrl.Result{Requeue: true}, nil
	}

	// Handle normal reconciliation loop.
	return r.reconcile(ctx, suRelease)
}

func (r *SylvaUnitsReleaseReconciler) reconcile(ctx context.Context, suRelease *sylvav1alpha1.SylvaUnitsRelease) (ctrl.Result, error) {
	resources := []client.Object{}

	// Set and create the namespace which will host the helm release defining the cluster.
	// It may be different based on the cluster type for instance
	helmReleaseNamespace := namespace.TargetClusterNamespace(suRelease)
	namespace, err := namespace.GenerateNamespace(suRelease, helmReleaseNamespace)
	if err != nil {
		return ctrl.Result{}, err
	}
	resNamespace, err := r.applyManagedResource(ctx, &namespace)
	if err != nil {
		return ctrl.Result{}, err
	}
	resources = append(resources, resNamespace)

	// Copy configmaps if necessary
	sourceConfigMap := &corev1.ConfigMap{}
	type configMapToCopy struct {
		configMapName        string
		sourceNamespace      string
		destinationNamespace string
	}
	configMapToCopyList := []configMapToCopy{}

	if suRelease.Spec.ClusterType == sylvav1alpha1.WorkloadClusterType {
		// Copy the managed-workload-clusters-settings from the team namespace
		configMapToCopyList = append(configMapToCopyList, configMapToCopy{
			configMapName:        "managed-workload-clusters-settings",
			sourceNamespace:      suRelease.Namespace,
			destinationNamespace: helmReleaseNamespace,
		})
	}

	for _, v := range configMapToCopyList {
		if err := r.Get(ctx, types.NamespacedName{Name: v.configMapName, Namespace: v.sourceNamespace}, sourceConfigMap); err != nil {
			return ctrl.Result{}, err
		}
		cm, err := configmap.CopyBetweenNamespace(sourceConfigMap, v.destinationNamespace)
		if err != nil {
			return ctrl.Result{}, err
		}
		resConfigMap, err := r.applyManagedResource(ctx, cm)
		if err != nil {
			return ctrl.Result{}, err
		}
		resources = append(resources, resConfigMap)
	}

	// Create the external secrets if necessary
	valuesFromList := []sylvav1alpha1.ValuesFrom{}

	type externalSecretToCreate struct {
		esClusterStoreNamespace string
		esTargetName            string
		esInLineTemplate        string
		esRefreshInterval       metav1.Duration
		esExternalPath          string
		esExternalKey           string
	}
	externalSecretToCreateList := []externalSecretToCreate{}

	if suRelease.Spec.ClusterType == sylvav1alpha1.WorkloadClusterType {
		if suRelease.Spec.SecretValues.Path != "" || suRelease.Spec.SecretValues.Key != "" {
			externalSecretToCreateList = append(externalSecretToCreateList, externalSecretToCreate{
				esClusterStoreNamespace: suRelease.Namespace,
				esTargetName:            "sylva-units-secrets",
				esInLineTemplate:        `{{- (regexReplaceAll "{{.*}}" .secrets "")` + ` | fromJson | toYaml }}`, // Prevent people from passing Helm template through secrets
				esRefreshInterval:       metav1.Duration{Duration: 30 * time.Second},
				esExternalPath:          suRelease.Spec.SecretValues.Path,
				esExternalKey:           suRelease.Spec.SecretValues.Key,
			})

			valuesFromItem := sylvav1alpha1.ValuesFrom{
				LayerName: "secret",
				Type:      sylvav1alpha1.ValuesFromTypeSecret,
				Name:      "sylva-units-secrets",
				ValuesKey: "secrets",
			}
			valuesFromList = append(valuesFromList, valuesFromItem)
		}
	}

	for _, v := range externalSecretToCreateList {
		externalSecret, err := externalsecrets.GenerateExternalSecret(
			v.esClusterStoreNamespace,
			helmReleaseNamespace,
			v.esTargetName,
			v.esInLineTemplate,
			v.esRefreshInterval,
			v.esExternalPath,
			v.esExternalKey,
		)
		if err != nil {
			return ctrl.Result{}, err
		}
		resExternalSecret, err := r.applyManagedResource(ctx, externalSecret)
		if err != nil {
			return ctrl.Result{}, err
		}
		resources = append(resources, resExternalSecret)
	}

	// Create the Helm Release
	if suRelease.Spec.SylvaUnitsSource.Type == sylvav1alpha1.SourceTypeGit {
		gitRepo, err := gitrepository.GenerateGitRepository(suRelease, helmReleaseNamespace)
		if err != nil {
			return ctrl.Result{}, err
		}
		resGitRepository, err := r.applyManagedResource(ctx, gitRepo)
		if err != nil {
			return ctrl.Result{}, err
		}
		resources = append(resources, resGitRepository)
	} else { // SourceTypeOCI
		helmRepo, err := helmrepository.GenerateHelmRepository(suRelease, helmReleaseNamespace)
		if err != nil {
			return ctrl.Result{}, err
		}
		resHelmRepository, err := r.applyManagedResource(ctx, helmRepo)
		if err != nil {
			return ctrl.Result{}, err
		}
		resources = append(resources, resHelmRepository)
	}

	helmRelease, err := helmrelease.GenerateHelmRelease(suRelease, helmReleaseNamespace, valuesFromList)
	if err != nil {
		return ctrl.Result{}, err
	}
	resHelmRelease, err := r.applyManagedResource(ctx, helmRelease)
	if err != nil {
		return ctrl.Result{}, err
	}
	resources = append(resources, resHelmRelease)

	// Add sylva-units-status kustomization to the list of dependent resources
	// required to be Ready to consider sylvaUnitsRelease as Ready
	suStatus, err := r.getSylvaUnitsStatus(ctx, helmReleaseNamespace)
	if err != nil {
		return ctrl.Result{}, err
	}
	resources = append(resources, suStatus)

	// Check for managed resources status
	if r.checkManagedResources(suRelease, resources) {
		conditions.MarkTrue(suRelease, sylvav1alpha1.ReadyCondition, sylvav1alpha1.ReconciliationSucceededReason, "Successfully reconciled sylvaUnitsRelease")
		return ctrl.Result{}, nil
	} else {
		conditions.MarkFalse(suRelease, sylvav1alpha1.ReadyCondition, sylvav1alpha1.ResourceNotReadyReason, "Some resources are not Ready")
		return ctrl.Result{RequeueAfter: resourcePollingInterval}, nil
	}
}

func (r *SylvaUnitsReleaseReconciler) reconcileDelete(ctx context.Context, suRelease *sylvav1alpha1.SylvaUnitsRelease) (ctrl.Result, error) {
	resources := []client.Object{}
	helmReleaseNamespace := namespace.TargetClusterNamespace(suRelease)
	namespacedName := types.NamespacedName{Name: sylvav1alpha1.BaseSylvaUnitsResourceName, Namespace: helmReleaseNamespace}
	if res, err := r.deleteManagedResource(ctx, &helmv2.HelmRelease{}, namespacedName); err != nil {
		return ctrl.Result{}, err
	} else if res != nil {
		resources = append(resources, res)
	}
	if suRelease.Spec.SylvaUnitsSource.Type == sylvav1alpha1.SourceTypeGit {
		if res, err := r.deleteManagedResource(ctx, &sourcev1.GitRepository{}, namespacedName); err != nil {
			return ctrl.Result{}, err
		} else if res != nil {
			resources = append(resources, res)
		}
	} else { // SourceTypeOCI
		if res, err := r.deleteManagedResource(ctx, &sourcev1b2.HelmRepository{}, namespacedName); err != nil {
			return ctrl.Result{}, err
		} else if res != nil {
			resources = append(resources, res)
		}
	}
	if res, err := r.deleteManagedResource(ctx, &esov1b1.ExternalSecret{}, namespacedName); err != nil {
		return ctrl.Result{}, err
	} else if res != nil {
		resources = append(resources, res)
	}
	if res, err := r.deleteManagedResource(ctx, &corev1.Namespace{}, types.NamespacedName{Name: helmReleaseNamespace, Namespace: ""}); err != nil {
		return ctrl.Result{}, err
	} else if res != nil {
		resources = append(resources, res)
	}
	if len(resources) == 0 {
		controllerutil.RemoveFinalizer(suRelease, sylvav1alpha1.SylvaUnitsFinalizer)
		r.Eventf(suRelease, "Normal", "ReconciliationSucceeded", "Successfully deleted SylvaUnitsRelease %s", suRelease.Name)
		return ctrl.Result{}, nil
	} else {
		conditions.MarkFalse(suRelease, sylvav1alpha1.ReadyCondition, sylvav1alpha1.ResourcePruningReason, "Resources are being deleted")
		r.checkManagedResources(suRelease, resources)
		return ctrl.Result{RequeueAfter: resourcePollingInterval}, nil
	}
}

func (r *SylvaUnitsReleaseReconciler) applyManagedResource(ctx context.Context, obj client.Object) (client.Object, error) {
	namespacedName := client.ObjectKeyFromObject(obj)
	objKind, err := r.Client.GroupVersionKindFor(obj)
	if err != nil {
		return nil, err
	}
	log := ctrl.LoggerFrom(ctx).WithValues("Kind", objKind.Kind, "NamespacedName", namespacedName)
	// Patch object using server-side apply strategy
	obj.GetObjectKind().SetGroupVersionKind(objKind)
	if err := r.Client.Patch(ctx, obj, client.Apply, client.ForceOwnership, client.FieldOwner(fieldOwner)); err != nil {
		return obj, fmt.Errorf("failed to patch %s %s: %w", objKind.Kind, namespacedName.String(), err)
	}
	log.Info(fmt.Sprintf("Applied desired state to %s %s", objKind.Kind, namespacedName.String()))
	return obj, nil
}

func (r *SylvaUnitsReleaseReconciler) deleteManagedResource(
	ctx context.Context,
	obj client.Object,
	namespacedName types.NamespacedName) (client.Object, error) {

	objKind, err := r.Client.GroupVersionKindFor(obj)
	if err != nil {
		return nil, err
	}
	log := ctrl.LoggerFrom(ctx).WithValues("Kind", objKind.Kind, "NamespacedName", namespacedName)

	if err := r.Get(ctx, namespacedName, obj); err != nil {
		switch err.(type) {
		case *meta.NoKindMatchError:
			log.Info("No matches for the kind. Probably the related CRD is not installed")
			return nil, nil
		default:
			if apierrors.IsNotFound(err) {
				log.Info("Successfully deleted flux object")
				return nil, nil
			}
			return nil, fmt.Errorf("failed to retrieving %s %s for deletion: %w", objKind.Kind, namespacedName.String(), err)
		}
	}

	switch objKind.Kind {
	case "Namespace":
		if err := r.deleteNamespace(ctx, obj); err != nil {
			if apierrors.IsNotFound(err) {
				// Object has been deleted in the meantime
				return nil, nil
			}
			return nil, fmt.Errorf("failed to delete %s %s: %w", objKind.Kind, namespacedName.String(), err)
		}
		return obj, nil
	default:
		if err := r.Delete(ctx, obj); err != nil {
			if apierrors.IsNotFound(err) {
				// Object has been deleted in the meantime
				return nil, nil
			}
			return nil, fmt.Errorf("failed to delete %s %s: %w", objKind.Kind, namespacedName.String(), err)
		}
		return obj, nil
	}
}

func (r *SylvaUnitsReleaseReconciler) deleteNamespace(ctx context.Context, obj client.Object) error {
	namespacedName := client.ObjectKeyFromObject(obj)
	objKind, err := r.Client.GroupVersionKindFor(obj)
	if err != nil {
		return err
	}
	log := ctrl.LoggerFrom(ctx).WithValues("Kind", objKind.Kind, "NamespacedName", namespacedName)
	// Ensure that there is no more ressources in the namespace
	opts := []client.ListOption{
		client.InNamespace(namespacedName.Name),
	}
	helmreleases := &helmv2.HelmReleaseList{}
	if err := r.List(ctx, helmreleases, opts...); err != nil {

		switch err.(type) {
		case *meta.NoKindMatchError:
			log.Info("No matches for the kind Helmrelease. Probably the CRD is not installed")
		default:
			if !apierrors.IsNotFound(err) {
				return fmt.Errorf("failed to retrieving required resources to delete before the deletion of %s %s: %w", objKind.Kind, namespacedName.String(), err)
			}
		}
	} else if len(helmreleases.Items) > 0 {
		log.Info("Still deleting required helmreleases")
		return nil
	}

	// Delete the empty namespaces

	if err := r.Delete(ctx, obj); err != nil {

		if apierrors.IsNotFound(err) {

			// Object has been deleted in the meantime
			return nil
		}
		return fmt.Errorf("failed to delete %s %s: %w", objKind.Kind, namespacedName.String(), err)
	}
	return nil
}

func (r *SylvaUnitsReleaseReconciler) checkManagedResources(suRelease *sylvav1alpha1.SylvaUnitsRelease, managedResources []client.Object) bool {
	// Mark the object as ready by default
	resourcesStatus := []sylvav1alpha1.ResourceStatus{}
	resourcesReady := true
	for _, resource := range managedResources {
		resourceStatus := sylvav1alpha1.ResourceStatus{
			Kind: resource.GetObjectKind().GroupVersionKind().Kind,
			Name: resource.GetName(),
		}
		// Convert object to Unstructured
		obj, err := runtime.DefaultUnstructuredConverter.ToUnstructured(resource)
		if err != nil {
			resourcesReady = false
			resourceStatus.Status = fmt.Sprintf(
				"Failed to convert %s %s to unstructured: %s",
				resource.GetObjectKind().GroupVersionKind().Kind,
				resource.GetName(),
				err)
			continue
		}
		u := &unstructured.Unstructured{Object: obj}
		// Build resourceStatus for object
		result, err := status.Compute(u)
		if err != nil {
			resourcesReady = false
			resourceStatus.Status = fmt.Sprintf("Failed to compute status: %s", err)
		} else {
			if result.Status == status.CurrentStatus {
				resourceStatus.Status = "Resource is Ready"
			} else {
				resourcesReady = false
				resourceStatus.Status = fmt.Sprintf("%s - %s", result.Status, result.Message)
			}
		}
		resourcesStatus = append(resourcesStatus, resourceStatus)
	}
	suRelease.Status.ResourcesStatus = resourcesStatus
	return resourcesReady
}

func (r *SylvaUnitsReleaseReconciler) getSylvaUnitsStatus(ctx context.Context, helmReleaseNamespace string) (client.Object, error) {
	namespacedName := types.NamespacedName{Name: sylvaUnitsStatusName, Namespace: helmReleaseNamespace}
	suStatus := &kustomizev1.Kustomization{}
	// Set minimal info to make sure that kstatus won't consider that resource is ready if it is not found
	suStatus.SetName(sylvaUnitsStatusName)
	conditions.MarkFalse(suStatus, sylvav1alpha1.ReadyCondition, sylvav1alpha1.ResourceNotFoundReason, "Resource not found")
	suStatus.Status.ObservedGeneration = -1

	err := r.Get(ctx, namespacedName, suStatus)
	if err != nil && !apierrors.IsNotFound(err) {
		return nil, err
	}
	return suStatus, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *SylvaUnitsReleaseReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&sylvav1alpha1.SylvaUnitsRelease{}).
		Complete(r)
}
