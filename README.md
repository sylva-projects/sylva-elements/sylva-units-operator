# sylva-units-operator

## Intro

This repository contains code for a Kubernetes `SylvaUnitsRelease` CRD and operator for the definition of
a FluxCD HelmRelease of the [sylva-units Helm chart](https://gitlab.com/sylva-projects/sylva-core/-/tree/main/charts/sylva-units),
and of the objects associated to it: ConfigMaps and Secrets used in valuesFrom, GitRepository or HelmRepository used
to get the sylva-units Helm chart.

The code producing the manifests is mean to be usable as a Go module to make it possible
to benefit from the facilities implemented to produce those HelmReleases from a CLI
usable without a Kubernetes cluster, for instance in [`sylvactl`](https://gitlab.com/sylva-projects/sylva-elements/sylvactl).
