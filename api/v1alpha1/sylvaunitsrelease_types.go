/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"encoding/json"

	apiextensionsv1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	helmv2 "github.com/fluxcd/helm-controller/api/v2beta1"
)

const (
	// used for SylvaUnitsRelease.Spec.ClusterType:
	ManagementClusterType = "management"
	WorkloadClusterType   = "workload"
	BootstrapClusterType  = "bootstrap"

	// Used in SylvaUnitsRelease.Spec.Source.Type
	SourceTypeOCI = "oci"
	SourceTypeGit = "git"

	// Used in SylvaUnitsRelease.Spec.ValuesFrom[].Type
	ValuesFromTypeConfigMap = "ConfigMap"
	ValuesFromTypeSecret    = "Secret"

	SylvaUnitsFinalizer = "sylva-units-operator.sylva/finalizer"

	BaseSylvaUnitsResourceName = "sylva-units"
)

// SylvaUnitsReleaseSpec defines the desired state of SylvaUnitsRelease
type SylvaUnitsReleaseSpec struct {

	// ClusterType defines the type of Sylva cluster that this release
	// of sylva-units will manage.  It governs the pre-selection of values.xxx.yaml
	// files from sylva-units Helm chart.
	// +kubebuilder:validation:Enum=management;workload;bootstrap
	// +required
	ClusterType string `json:"clusterType"`

	// +required
	SylvaUnitsSource *SylvaUnitsSource `json:"sylvaUnitsSource"`

	// +optional
	ValuesFrom []ValuesFrom `json:"valuesFrom,omitempty"`

	// Some fields are just the same as in a HelmRelease ////

	// Values holds the values for this Helm release.
	// +optional
	Values *apiextensionsv1.JSON `json:"values,omitempty"`

	// SecretValues define the (relative) path to a JSON secret and the key, stored in the Sylva secret storage (ex: /secret/data/${current namespace}/${SecretValues.path}/${SecretValues.key} in Vault).
	// +optional
	SecretValues SecretValuesSpec `json:"secretValues,omitempty"`

	// Interval at which the FluxCD resources are reconciled
	// +kubebuilder:validation:Type=string
	// +kubebuilder:validation:Pattern="^([0-9]+(\\.[0-9]+)?(ms|s|m|h))+$"
	// +kubebuilder:default="30m"
	// +optional
	Interval metav1.Duration `json:"interval"`

	// Suspend tells the controller to suspend reconciliation for this SylvaUnitsRelease,
	// +kubebuilder:default=false
	// +optional
	Suspend bool `json:"suspend,omitempty"`
}

// SylvaUnitsSource defines where to find the sylva-units Helm chart
// This is used to built HelmRelease.spec.chart
// and the objects it needs (OCIRepository or GitRepository)

// +kubebuilder:validation:XValidation:rule="!(self.type == \"oci\" && !has(self.tag))",message="tag is required for type oci"
// +kubebuilder:validation:XValidation:rule="!(self.type == \"oci\" && has(self.branch))",message="branch cannot be set for type oci"
// +kubebuilder:validation:XValidation:rule="!(self.type == \"oci\" && has(self.commit))",message="commit cannot be set for type oci"
// +kubebuilder:validation:XValidation:rule="!(self.type == \"oci\" && !(url(self.url).getScheme() == \"oci\"))",message="url scheme must be oci:// for a source of type oci"
// +kubebuilder:validation:XValidation:rule="!(self.type == \"git\" && !(has(self.tag) || has(self.branch) || has(self.commit)))",message="for type git, one of tag/branch/commit is required"
// +kubebuilder:validation:XValidation:rule="!(self.type == \"git\" && !(url(self.url).getScheme() == \"https\" || url(self.url).getScheme() == \"http\" || url(self.url).getScheme() == \"ssh\"))",message="url scheme must be http://,https:// or ssh:// for a source of type git"
type SylvaUnitsSource struct {
	// +kubebuilder:validation:Enum=oci;git
	// +kubebuilder:validation:XValidation:rule="self == oldSelf",message="Value is immutable"
	// +required
	Type string `json:"type"`

	// +required
	URL string `json:"url"`

	// contains a Git tag or for OCI deployments the version
	// of the Helm chart.
	// +optional
	Tag string `json:"tag,omitempty"`

	// contains a Git branch name (only for Git deployments)
	// +optional
	Branch string `json:"branch,omitempty"`

	// contains a Git commit id (only for Git deployments)
	// +optional
	Commit string `json:"commit,omitempty"`
}

// +kubebuilder:validation:XValidation:rule="!(self.type == \"ConfigMap\" && !has(self.name))",message="name is required for type ConfigMap"
// +kubebuilder:validation:XValidation:rule="!(self.type == \"Secret\" && !has(self.name))",message="name is required for type Secret"
type ValuesFrom struct {
	// +kubebuilder:validation:Pattern="^[a-zA-Z][a-zA-Z0-9-_]*[a-zA-Z0-9]$"
	// +kubebuilder:validation:MinLength=1
	// +kubebuilder:validation:MaxLength=253
	// +optional
	LayerName string `json:"layerName,omitempty"`

	// +kubebuilder:validation:Enum=ConfigMap;Secret;
	// +required
	Type string `json:"type"`

	// **** taken from helm-controller/api/v2beta1/reference_types.go ValuesReference ****/

	// Contains the name of the resource to read values from
	// For use with types ConfigMap and Secret
	// +kubebuilder:validation:Pattern="^[a-zA-Z][a-zA-Z0-9-_]*[a-zA-Z0-9]$"
	// +kubebuilder:validation:MinLength=1
	// +kubebuilder:validation:MaxLength=253
	// +optional
	Name string `json:"name,omitempty"`

	// ValuesKey is the data key where the values.yaml or a specific value can be
	// found at. Defaults to 'values.yaml'.
	// When set, must be a valid Data Key, consisting of alphanumeric characters,
	// '-', '_' or '.'.
	// +kubebuilder:validation:MaxLength=253
	// +kubebuilder:validation:Pattern=`^[\-._a-zA-Z0-9]+$`
	// +optional
	ValuesKey string `json:"valuesKey,omitempty"`

	// TargetPath is the YAML dot notation path the value should be merged at. When
	// set, the ValuesKey is expected to be a single flat value. Defaults to 'None',
	// which results in the values getting merged at the root.
	// +kubebuilder:validation:MaxLength=250
	// +kubebuilder:validation:Pattern=`^([a-zA-Z0-9_\-.\\\/]|\[[0-9]{1,5}\])+$`
	// +optional
	TargetPath string `json:"targetPath,omitempty"`

	// Optional marks this ValuesReference as optional. When set, a not found error
	// for the values reference is ignored, but any ValuesKey, TargetPath or
	// transient error will still result in a reconciliation failure.
	// +optional
	Optional bool `json:"optional,omitempty"`

	// *************************************************************************************

	// +optional
	URL string `json:"url,omitempty"`
}

// Define the relative path / key to access to the Sylva secret storage. If not defined, no secret is mounted
type SecretValuesSpec struct {
	// +optional
	Path string `json:"path"`

	// +optional
	Key string `json:"key"`
}

// SylvaUnitsReleaseStatus defines the observed state of SylvaUnitsRelease
type SylvaUnitsReleaseStatus struct {
	// ObservedGeneration is the last observed generation.
	// +optional
	ObservedGeneration int64 `json:"observedGeneration,omitempty"`

	// +optional
	Conditions []metav1.Condition `json:"conditions,omitempty"`

	// This is the status of the generated HelmRelease
	// +optional
	HelmReleaseStatus helmv2.HelmReleaseStatus `json:"helmReleaseStatus,omitempty"`

	// This is the status of managed resources
	// +optional
	ResourcesStatus []ResourceStatus `json:"resourcesStatus,omitempty"`
}

// ResourceStatus provides information on managed resources
type ResourceStatus struct {
	// Kind of the resource
	Kind string `json:"kind"`

	// Name of the resource
	Name string `json:"name"`

	// Status of the resource
	Status string `json:"status"`
}

// GetConditions returns the status conditions of the object.
func (in SylvaUnitsRelease) GetConditions() []metav1.Condition {
	return in.Status.Conditions
}

// SetConditions sets the status conditions on the object.
func (in *SylvaUnitsRelease) SetConditions(conditions []metav1.Condition) {
	in.Status.Conditions = conditions
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status
//+kubebuilder:resource:shortName=sur

// SylvaUnitsRelease is the Schema for the sylvaunitsrelease API
type SylvaUnitsRelease struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec SylvaUnitsReleaseSpec `json:"spec,omitempty"`
	// +kubebuilder:default:={"observedGeneration":-1}
	Status SylvaUnitsReleaseStatus `json:"status,omitempty"`
}

// GetValues unmarshals the raw values to a map[string]interface{} and returns
// the result.
func (in SylvaUnitsRelease) GetValues() map[string]interface{} {
	values := map[string]interface{}{}
	if in.Spec.Values != nil {
		_ = json.Unmarshal(in.Spec.Values.Raw, &values)
	}
	return values
}

//+kubebuilder:object:root=true

// SylvaUnitsReleaseList contains a list of SylvaUnitsRelease
type SylvaUnitsReleaseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []SylvaUnitsRelease `json:"items"`
}

//+kubebuilder:object:root=true

// SylvaUnitsReleaseTemplate
// This resources holds a template for a SylvaUnitsRelease.
//
// It has the same specification as a SylvaUnitsRelease.
//
// It isn't subject to any controller processing, but can hold
// information from which a SylvaUnitsRelease that refers to it
// will inherit.
type SylvaUnitsReleaseTemplate struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec SylvaUnitsReleaseSpec `json:"spec,omitempty"`
}

//+kubebuilder:object:root=true

// SylvaUnitsReleaseList contains a list of SylvaUnitsRelease
type SylvaUnitsReleaseTemplateList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []SylvaUnitsReleaseTemplate `json:"items"`
}

func init() {
	SchemeBuilder.Register(&SylvaUnitsRelease{}, &SylvaUnitsReleaseList{})
	SchemeBuilder.Register(&SylvaUnitsReleaseTemplate{}, &SylvaUnitsReleaseTemplateList{})
}
