/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

const (
	// ReadyCondition indicates the resource is ready and fully reconciled.
	ReadyCondition string = "Ready"

	// ResourcePruningReason represents the fact that
	// one of the managed resources  are being pruned.
	ResourcePruningReason string = "ResourcePruningReason"

	// ResourceNotReadyReason represents the fact that
	// one of the managed resources is not found.
	ResourceNotFoundReason string = "ResourceNotFoundReason"

	// ResourceNotReadyReason represents the fact that
	// one of the managed resources is not ready.
	ResourceNotReadyReason string = "ResourceNotReady"

	// ReconciliationSucceededReason represents the fact that
	// the reconciliation succeeded.
	ReconciliationSucceededReason string = "ReconciliationSucceeded"

	// ReconciliationFailedReason represents the fact that
	// the reconciliation failed.
	ReconciliationFailedReason string = "ReconciliationFailed"
)
