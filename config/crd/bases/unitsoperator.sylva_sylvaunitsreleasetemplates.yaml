---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.14.0
  name: sylvaunitsreleasetemplates.unitsoperator.sylva
spec:
  group: unitsoperator.sylva
  names:
    kind: SylvaUnitsReleaseTemplate
    listKind: SylvaUnitsReleaseTemplateList
    plural: sylvaunitsreleasetemplates
    singular: sylvaunitsreleasetemplate
  scope: Namespaced
  versions:
  - name: v1alpha1
    schema:
      openAPIV3Schema:
        description: |-
          SylvaUnitsReleaseTemplate
          This resources holds a template for a SylvaUnitsRelease.


          It has the same specification as a SylvaUnitsRelease.


          It isn't subject to any controller processing, but can hold
          information from which a SylvaUnitsRelease that refers to it
          will inherit.
        properties:
          apiVersion:
            description: |-
              APIVersion defines the versioned schema of this representation of an object.
              Servers should convert recognized schemas to the latest internal value, and
              may reject unrecognized values.
              More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources
            type: string
          kind:
            description: |-
              Kind is a string value representing the REST resource this object represents.
              Servers may infer this from the endpoint the client submits requests to.
              Cannot be updated.
              In CamelCase.
              More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds
            type: string
          metadata:
            type: object
          spec:
            description: SylvaUnitsReleaseSpec defines the desired state of SylvaUnitsRelease
            properties:
              clusterType:
                description: |-
                  ClusterType defines the type of Sylva cluster that this release
                  of sylva-units will manage.  It governs the pre-selection of values.xxx.yaml
                  files from sylva-units Helm chart.
                enum:
                - management
                - workload
                - bootstrap
                type: string
              interval:
                default: 30m
                description: Interval at which the FluxCD resources are reconciled
                pattern: ^([0-9]+(\.[0-9]+)?(ms|s|m|h))+$
                type: string
              secretValues:
                description: 'SecretValues define the (relative) path to a JSON secret
                  and the key, stored in the Sylva secret storage (ex: /secret/data/${current
                  namespace}/${SecretValues.path}/${SecretValues.key} in Vault).'
                properties:
                  key:
                    type: string
                  path:
                    type: string
                type: object
              suspend:
                default: false
                description: Suspend tells the controller to suspend reconciliation
                  for this SylvaUnitsRelease,
                type: boolean
              sylvaUnitsSource:
                properties:
                  branch:
                    description: contains a Git branch name (only for Git deployments)
                    type: string
                  commit:
                    description: contains a Git commit id (only for Git deployments)
                    type: string
                  tag:
                    description: |-
                      contains a Git tag or for OCI deployments the version
                      of the Helm chart.
                    type: string
                  type:
                    enum:
                    - oci
                    - git
                    type: string
                    x-kubernetes-validations:
                    - message: Value is immutable
                      rule: self == oldSelf
                  url:
                    type: string
                required:
                - type
                - url
                type: object
                x-kubernetes-validations:
                - message: tag is required for type oci
                  rule: '!(self.type == "oci" && !has(self.tag))'
                - message: branch cannot be set for type oci
                  rule: '!(self.type == "oci" && has(self.branch))'
                - message: commit cannot be set for type oci
                  rule: '!(self.type == "oci" && has(self.commit))'
                - message: url scheme must be oci:// for a source of type oci
                  rule: '!(self.type == "oci" && !(url(self.url).getScheme() == "oci"))'
                - message: for type git, one of tag/branch/commit is required
                  rule: '!(self.type == "git" && !(has(self.tag) || has(self.branch)
                    || has(self.commit)))'
                - message: url scheme must be http://,https:// or ssh:// for a source
                    of type git
                  rule: '!(self.type == "git" && !(url(self.url).getScheme() == "https"
                    || url(self.url).getScheme() == "http" || url(self.url).getScheme()
                    == "ssh"))'
              values:
                description: Values holds the values for this Helm release.
                x-kubernetes-preserve-unknown-fields: true
              valuesFrom:
                items:
                  properties:
                    layerName:
                      maxLength: 253
                      minLength: 1
                      pattern: ^[a-zA-Z][a-zA-Z0-9-_]*[a-zA-Z0-9]$
                      type: string
                    name:
                      description: |-
                        Contains the name of the resource to read values from
                        For use with types ConfigMap and Secret
                      maxLength: 253
                      minLength: 1
                      pattern: ^[a-zA-Z][a-zA-Z0-9-_]*[a-zA-Z0-9]$
                      type: string
                    optional:
                      description: |-
                        Optional marks this ValuesReference as optional. When set, a not found error
                        for the values reference is ignored, but any ValuesKey, TargetPath or
                        transient error will still result in a reconciliation failure.
                      type: boolean
                    targetPath:
                      description: |-
                        TargetPath is the YAML dot notation path the value should be merged at. When
                        set, the ValuesKey is expected to be a single flat value. Defaults to 'None',
                        which results in the values getting merged at the root.
                      maxLength: 250
                      pattern: ^([a-zA-Z0-9_\-.\\\/]|\[[0-9]{1,5}\])+$
                      type: string
                    type:
                      enum:
                      - ConfigMap
                      - Secret
                      type: string
                    url:
                      type: string
                    valuesKey:
                      description: |-
                        ValuesKey is the data key where the values.yaml or a specific value can be
                        found at. Defaults to 'values.yaml'.
                        When set, must be a valid Data Key, consisting of alphanumeric characters,
                        '-', '_' or '.'.
                      maxLength: 253
                      pattern: ^[\-._a-zA-Z0-9]+$
                      type: string
                  required:
                  - type
                  type: object
                  x-kubernetes-validations:
                  - message: name is required for type ConfigMap
                    rule: '!(self.type == "ConfigMap" && !has(self.name))'
                  - message: name is required for type Secret
                    rule: '!(self.type == "Secret" && !has(self.name))'
                type: array
            required:
            - clusterType
            - sylvaUnitsSource
            type: object
        type: object
    served: true
    storage: true
